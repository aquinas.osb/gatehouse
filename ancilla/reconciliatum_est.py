"""
reconciliatum_est.py -- Finish reconciliation of inventory by updating recorded sales
"""

import os

import ancilla.cor
import ancilla.args
import ancilla.xl

def main():

    parser = ancilla.args.AncillaParser(input_file_filter=(('Inventory Snapshot (inventory_snapshot_*.xlsx)','inventory_snapshot_*.xlsx'),),
                                        intro_callback=intro)
    args = parser.parse_args()

    live_inventory = ancilla.cor.load_live_inventory(args.inventory)

    purchased = ancilla.cor.transaction("PURCHASE")
    sold      = ancilla.cor.transaction("SALE")

    for f in args.input_files:
        for table in ancilla.xl.load_workbook_tables(f):
            print('loading table',table.title)

            diff_header = table.headers[2]
            stock_type = diff_header[6:-1] # 'Diff (G)'  TODO: more robust way to do this...

            for row in table.data:
                code = row['code']
                diff = int(row[diff_header])

                if diff > 0:
                    if False: # purchases not supported currently (TODO)
                        purchased.add_item(code,diff)
                        print("Product %s '%s': add %i Qty Purchased." % (code,
                                                                          row['title'],
                                                                          diff))
                elif diff < 0:
                    if ancilla.xl.bool_val(row['MARK SALE']):
                        sold.add_item(code,-diff)
                        print("Product %s '%s': add %i Qty Sold." % (code,
                                                                     row['title'],
                                                                     -diff))

    print('====')
    print('Are you ready? Check above to make sure you really want to load all those transactions')
    print("(Note: The Purchase and Sale numbers will be updated but the stock won't since it was already updated from the stock take)")
    os.system('pause') # todo: make this depend on a cmd switch
    print("Here we go...")

    if len(purchased) > 0:
        print(purchased)
        live_inventory.process_transaction(purchased, update_qty_stock=False)
    if len(sold) > 0:
        print(sold)
        live_inventory.process_transaction(sold, update_qty_stock=False)

    if len(purchased) == 0 and len(sold) == 0:
        print("NOTHING TO DO!")
    else:
        ancilla.cor.save_live_inventory(live_inventory)

    if args.yes_we_did_do_the_intro:
        outro()

def intro():
    print(ancilla.cor.shell_splash)
    print('========= ReconciliatumEst =========')
    print(__doc__)
    print('Woah there cowboy, when the first file chooser prompt comes up, select your inventory Excel file which has all live inventory and product data in it.  Then once you done that, another file prompt will come up and you have to select the inventory snapshot you just recently created. Are you ready? Okay then...')
    os.system('pause')

def outro():
    print("Done.")
