"""
aron_iumport.py -- Import Aronium reports to update live inventory
"""

import os

import ancilla.cor
import ancilla.args
import ancilla.xl

def main():

    parser = ancilla.args.AncillaParser(input_file_filter=(('Aronium Sales Report (*sales*.xlsx)','*sales*.xlsx'),),
                                        intro_callback=intro)
    args = parser.parse_args()

    live_inventory = ancilla.cor.load_live_inventory(args.inventory)

    for report_file in args.input_files:
        t = None
        for ttype, sku, name, qty in load_report_file(report_file):

            print(ttype, sku, name, qty)

            # assume the report represents one big transaction
            if t is None:
                t = ancilla.cor.transaction(ttype)

            i = live_inventory.get_or_add_item_by_aronium_sku(sku, name)
            t.add_item(i.product.code,qty)

        print(t)
        live_inventory.process_transaction(t)

    ancilla.cor.save_live_inventory(live_inventory)

    if args.yes_we_did_do_the_intro:
        outro()

def intro():
    print(ancilla.cor.shell_splash)
    print('========= AronIumport =========')
    print(__doc__)
    print('Woah there cowboy, when the first file chooser prompt comes up, select your inventory Excel file which has all live inventory and product data in it.  Then once you done that, another file prompt will come up and you have to select the file with Aronium report. Are you ready? Okay then...')
    os.system('pause')

def outro():
    print("Done.")

def load_report_file(report_file):

    sheet_tables = ancilla.xl.load_workbook_tables(report_file)

    for row_data in sheet_tables[0].data: # iterate through table (Assume Aronium report has only one sheet)

        ttype = 'SALE' # assume sales report for now
        sku   = row_data['Code']
        name  = row_data['Product']
        qty   = int(row_data['Quantity']) # if the file is malformed this will break. (hopefuly it wont be)

        yield ttype, sku, name, qty
