"""
Global variables which can be modified by the user to change default behaviour.
"""

###
### THE VARIABLES BELOW CAN BE CONFIGURED BY USER
###

# (please note that in Python the True and False values must be exactly capitalised as so.)

# if this is True, then a ProductNote will cause the program to ask you to type in a product description.
# if this is False, then a ProductNote will be given a default description (either way you can change it later
prompt_for_product_note = False

# if this is True, then stock-takes will increment the gstock_col or istock_col
# if this is False, then stock-takes will overwrite values in said columns. (any item not included in the stock-take will be 0'd)
stock_take_incrementally = False

###
### THE VARIABLES ABOVE CAN BE CONFIGURED BY USER
###
