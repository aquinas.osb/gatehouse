"""
pub_cypher_lector.py -- read barcode commands and update the inventory
"""

import os
import sys
import datetime
import csv
import re
import time

import ancilla.cor
import ancilla.args
import ancilla.xl

def main():

    args = setup_parser().parse_args()

    cmd_files = args.input_files

    if len(cmd_files) > 0:
        live_inventory = ancilla.cor.load_live_inventory(inventory_file=args.inventory)

        for cmd_file in cmd_files:
            with open(cmd_file, encoding='utf-8') as f:
                process_barcode_commands(live_inventory,
                                         parse_cmds(f))

        if not args.dry_run:
            ancilla.cor.save_live_inventory(live_inventory)

    if args.live_input:
        print("=== We're Live!!!! ===")
        print("Put the cursor on this window and then make scans from the barcode scanner. exit using ^Z")
        print("--")
        if args.dry_run and len(cmd_files) == 0:
            live_inventory = ancilla.cor.load_live_inventory(inventory_file=args.inventory)
        else:
            live_inventory = None
        process_barcode_commands(live_inventory,
                                 parse_cmds(sys.stdin, live_mode=True),
                                 save_between_cmds_to_file=(None if args.dry_run else args.inventory))

    if args.yes_we_did_do_the_intro:
        outro()

def setup_parser():

    parser = ancilla.args.AncillaParser(input_file_filter=(('Barcode Data (*.txt,*.log)',('*.txt','*.log')),),
                                        intro_callback=intro)

    parser.add_argument_which_overrides_file_chooser('--live-input', '-L', action='store_true',
            help='Read commands from stdin instead of from a file.',
            overrides_arg='input_files')

    return parser

def intro():
    print(ancilla.cor.shell_splash)
    print('====== PUB CYPHER LECTOR ======')
    print(__doc__)
    print('')
    #time.sleep(.5)
    print('BAR')
    #time.sleep(.5)
    print('CODE')
    #time.sleep(.5)
    print('READER')
    #time.sleep(1)
    print('(in case you didnt get it)')
    #time.sleep(.5)
    print('...')
    #time.sleep(.5)
    print('...')
    #time.sleep(.5)
    print('Woah there cowboy, when the first file chooser prompt comes up, select your inventory Excel file which has all live inventory and product data in it.  Then once you done that, another file prompt will come up and you have to select the file with the bar codes scans in it. Are you ready? Okay then...')
    os.system('pause')

def outro():
    # time.sleep(.5)
    # print('...')
    # time.sleep(.5)
    # print('...')
    # time.sleep(.5)
    # print('...')
    # time.sleep(.5)
    # print('*HACKER VOICE* "I\'m in."')
    # time.sleep(1)
    print("Done.")

cancel_cmd   = 'CANCEL'
complete_cmd = 'Complete'
break_cmd    = 'BREAK'

transaction_cmds = ancilla.cor.inventory.supported_transactions

begin_gatehouse_stocktake = 'Begin'+ancilla.cor.gatehouse_stock_type+'Stocktake'
pause_gatehouse_stocktake = 'Pause'+ancilla.cor.gatehouse_stock_type+'Stocktake'
resume_gatehouse_stocktake = 'Resume'+ancilla.cor.gatehouse_stock_type+'Stocktake'
end_gatehouse_stocktake = 'End'+ancilla.cor.gatehouse_stock_type+'Stocktake'

begin_online_stocktake = 'Begin'+ancilla.cor.online_stock_type+'Stocktake'
pause_online_stocktake = 'Pause'+ancilla.cor.online_stock_type+'Stocktake'
resume_online_stocktake = 'Resume'+ancilla.cor.online_stock_type+'Stocktake'
end_online_stocktake = 'End'+ancilla.cor.online_stock_type+'Stocktake'

paramless_cmds = [
    cancel_cmd,
    complete_cmd,
    break_cmd,
    end_gatehouse_stocktake,
    end_online_stocktake,
]

cmds_needing_inventory = transaction_cmds + [
    cancel_cmd,
    end_gatehouse_stocktake,
    end_online_stocktake,
]

product_note = 'ProductNote'
price_note = 'PriceNote'

delete_prev = 'Delete-Previous'

def process_barcode_commands(live_inventory, cmds, save_between_cmds_to_file=None):

    save_between_cmds = save_between_cmds_to_file is not None

    # if we aren't saving between commands, we assume the inventory was loaded externally
    # if we are, we assume it wasn't.
    live_inventory_loaded = not save_between_cmds

    transaction_records = ancilla.cor.load_transaction_records()

    stock_take_blocks = {ancilla.cor.gatehouse_stock_type: [],
                         ancilla.cor.online_stock_type: []}

    prev_transaction = None
    for cmd in cmds:

        # if the command requires the inventory to be loaded, make sure that it is
        if cmd.cmd in cmds_needing_inventory and not live_inventory_loaded:
            live_inventory = ancilla.cor.load_live_inventory(save_between_cmds_to_file)
            live_inventory_loaded = True

        # process commands
        if cmd.cmd in transaction_cmds:
            prev_transaction = record_transaction(cmd, live_inventory, transaction_records)

        elif cmd.cmd == cancel_cmd:
            cancel_transaction(prev_transaction, live_inventory)

        elif cmd.cmd == begin_gatehouse_stocktake:
            stock_take_blocks[ancilla.cor.gatehouse_stock_type] = [cmd]
        elif cmd.cmd == pause_gatehouse_stocktake:
            pass
        elif cmd.cmd == resume_gatehouse_stocktake:
            stock_take_blocks[ancilla.cor.gatehouse_stock_type].append(cmd)
        elif cmd.cmd == end_gatehouse_stocktake:
            do_stock_take(ancilla.cor.gatehouse_stock_type,
                          stock_take_blocks[ancilla.cor.gatehouse_stock_type],
                          cmd.timestamp,
                          live_inventory)

        elif cmd.cmd == begin_online_stocktake:
            stock_take_blocks[ancilla.cor.online_stock_type] = [cmd]
        elif cmd.cmd == pause_online_stocktake:
            pass
        elif cmd.cmd == resume_online_stocktake:
            stock_take_blocks[ancilla.cor.online_stock_type].append(cmd)
        elif cmd.cmd == end_online_stocktake:
            do_stock_take(ancilla.cor.online_stock_type,
                          stock_take_blocks[ancilla.cor.online_stock_type],
                          cmd.timestamp,
                          live_inventory)

        elif cmd.cmd == complete_cmd:
            # does nothing for itself, but does save the inventory if needed
            if live_inventory_loaded:
                ancilla.cor.save_live_inventory(live_inventory,
                                                save_between_cmds_to_file)
                live_inventory_loaded = False

        elif cmd.cmd == break_cmd:
            print(":: Exiting Command loop...")
            break

        elif cmd.cmd is None:
            print("MISSING", cmd)
        else:
            print("UNRECOGNISED", cmd)

        if (cmd.cmd not in transaction_cmds and
            cmd.cmd != complete_cmd):
            prev_transaction = None

    for rec in transaction_records:
        rec.save_file()

CMD_MARKER_US = '@'
QTY_MARKER_US = '~'
CMD_MARKER_IE = '"'
QTY_MARKER_IE = '¬'

class barcode_cmd:
    def __init__(self,cmd,timestamp):
        self.cmd       = cmd
        self.timestamp = timestamp
        self.params    = []

    def append(self,code,timestamp):
        self.params.append(code)
        if timestamp is not None:
            self.timestamp = timestamp
        return self

    @property
    def tag_groups(self):

        last_param, last_tags = None, []
        for param in self.params:

            if param.startswith(QTY_MARKER_US) and product_note not in param:
                last_tags.append(param.strip(QTY_MARKER_US).strip()) # continue set, strip the tag marker
            else:
                if (last_param is not None) or (len(last_tags) > 0):
                    yield (last_param, last_tags) # complete last set
                last_param, last_tags = param, [] #start new

        yield (last_param, last_tags) # complete final set

    def __str__(self):
        return "CMD[%s : %s @ %s]" % (str(self.cmd),
                                      str(tuple(self.params)),
                                      str(self.timestamp))

date_re = re.compile(r'[0-9]{4}/[0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]')
def parse_cmds(src, live_mode=False):
    next_command = None
    for line in src:

        line = line.replace(CMD_MARKER_IE, CMD_MARKER_US);
        line = line.replace(QTY_MARKER_IE, QTY_MARKER_US);

        m = date_re.search(line)

        timestamp = None
        if m is not None:
            timestamp = datetime.datetime.strptime(m.group(0),'%Y/%m/%d %H:%M:%S')
        elif live_mode:
            # if in live mode, use the actual time when timestamp is lacking
            timestamp = datetime.datetime.now()

        line = date_re.sub('',line).strip() # remove timestamp and use what remains

        if len(line) > 0:

            if line.startswith(CMD_MARKER_US):

                # finish off any command that is currently being process, and start the new one
                if next_command is not None:
                    yield next_command
                next_command = barcode_cmd(line.strip(CMD_MARKER_US),timestamp)

                # immediately yield commands that do not take parameters
                if next_command.cmd in paramless_cmds:
                    yield next_command
                    next_command = None

            elif next_command is not None:
                # add parameter to exisiting command
                next_command.append(line,timestamp)

            else:
                # yield a "null" command
                yield barcode_cmd(None,None).append(line,timestamp)

    if next_command is not None:
        yield next_command

def do_stock_take(stock_type, cmds, timestamp, live_inventory):
    new_stock_inventory =  ancilla.cor.inventory()

    if timestamp is None:
        for c in cmds:
            if c.timestamp is not None:
                timestamp = c.timestamp

    # load data from stock take
    total_items = 0
    for cmd in cmds:
        for param, tags in cmd.tag_groups:

            if param is None:
                if len(tags) > 0:
                    print('ACHTUNG: Ignoring these tags becasue no barcode is specified: %s' % tags)
                # else pass

            elif delete_prev in tags:
                print("Dropping parameter: %s because it has a %s tag: %s." % (param,delete_prev,tags))

            else:
                param_item = new_stock_inventory.stock_take_add(param, stock_type)
                total_items += 1
                # print('OK! adding one of these:', param)

                first_tag = True
                for tag in tags:

                    try:
                        qty = int(tag) # TODO: handle malformed int
                    except:
                        print("unrecognized ~ tag: %s" % tag)
                        continue

                    if first_tag:
                        qty -= 1 # to account for the first qty being added with barcode itself
                    first_tag = False

                    param_item.qty_stock[stock_type] += qty
                    total_items += qty
                    # print('Adding %s more of %s' % (qty, param_item.product.code))

    # get expected stock quantities from live inventory
    # store the expected value for the given stock type in an unused stock type
    available_stock_types = list(list(new_stock_inventory.all_items)[0].qty_stock.keys())
    expected_qty_stock_type = available_stock_types[1 if (available_stock_types[0] == stock_type) else 0]
    new_stock_inventory.update_from_stock_take(live_inventory,
                                               expected_qty_stock_type,
                                               _but_actually_this_stock_type=stock_type)

    # create inventory file
    filetime = 'unknowntime' if timestamp is None else timestamp.strftime(ancilla.cor.FILE_STRFTIME)
    filename = 'inventory_snapshot_%s_%s.xlsx' % (stock_type, filetime)

    print("::%sStocktake: @ %s" % (stock_type, str(timestamp)))
    print("::  %i items in %i pieces" % (total_items, len(cmds)))
    print("::  writing file: %s" % filename)
    
    snapshot_headers = [
        "Qty Stock (%s)" % stock_type,
        "Exp Qty (%s)" % stock_type,
        "Diff (%s)" % stock_type,       # C
        "title",
        "author",
        "code",
        "code alternate",
        "Aronium SKU",
        "price_base",                   # I
        "tax_rate",                     # J
        "Price of Diff inc tax",
        "MARK SALE"
    ]

    # (price_base + (price_base * tax_rate)) * Diff
    # note: {0} will be the current row number re-used here 4 times
    price_of_diff_inc_tax_formula = "=(I{0}+(I{0}*J{0}))*C{0}"

    wb = {}
    ws = None
    for item in new_stock_inventory.all_items:

        # determine which sheet to add the item to, create it if necessary
        sheet_title = item.product.xl_sheet
        if sheet_title is None:
            sheet_title = "NEW"

        if ws is None or ws.title != sheet_title:

            if sheet_title not in wb:
                wb[sheet_title] = ancilla.xl.SheetTable(sheet_title,
                                                        snapshot_headers)
            ws = wb[sheet_title]

        # then add the row of data
        qty_diff = item.qty_stock[stock_type] - item.qty_stock[expected_qty_stock_type]
        ws.append([
            item.qty_stock[stock_type],
            item.qty_stock[expected_qty_stock_type],
            qty_diff,
            str(item.product.title),
            str(item.product.author),
            str(item.product.code),
            str(item.product.code_alt),
            str(item.product.aronium_sku),
            item.product.price_base,
            item.product.tax_rate,
            price_of_diff_inc_tax_formula.format(ws.next_row_num)
        ])

    ancilla.xl.save_workbook_tables(wb.values(), filename)

    # update live inventory
    print("::  updating live inventory with new quantity for %s Stock." % stock_type)
    live_inventory.update_from_stock_take(new_stock_inventory, stock_type)

def record_transaction(cmd, inv, transaction_records):

    # construct a new transaction object from the command
    new_transaction = ancilla.cor.transaction(cmd.cmd,cmd.timestamp)

    pn_label_seq_num = 1
    for param, tags in cmd.tag_groups:

        if param is None:
            if len(tags) > 0:
                print("ACHTUNG: In %s, ignoring tag %s because it doesnt follow anything" % (cmd.cmd,tags))
            # else pass

        elif delete_prev in tags:
            print("Dropping parameter: %s because it has a %s tag: %s." % (param,delete_prev,tags))

        else:

            try_alt = True
            if param.startswith(QTY_MARKER_US+product_note):

                print("== WOAH! Product Note! ==")
                print("")
                print("Do you remember this one?")
                print(":: timestamp:", new_transaction.timestamp)
                print("")

                time = '' if new_transaction.timestamp is None else new_transaction.timestamp.strftime('%m-%d_%H:%M:%S')
                if ancilla.cor.prompt_for_product_note:
                    title = input('Write a discription here (you can come back to this later):').strip()
                    code  = "PN_%s_%s_%s" % (time, pn_label_seq_num, ''.join(title.split()))
                else:
                    title = "%s %s" % (product_note, time)
                    code  = "PN_%s_%s" % (time, pn_label_seq_num)
                inv._add_new_item(code)
                inv.items[code].product.title = title
                pn_label_seq_num += 1

                print("Great! We've made a new product!")
                print(":: code:",code)
                print(":: title:",title)
                print("")

                param = code
                try_alt = False

            new_transaction.add_item(param, try_alt=try_alt)


            # assumes all tags are quantities at this point
            first_tag = True
            for tag in tags:

                try:
                    qty = int(tag) # TODO: handle malformed int
                except:
                    print("unrecognized ~ tag: %s" % tag)
                    continue

                if first_tag:
                    qty -= 1 # to account for the first qty being added with barcode itself
                    first_tag = False

                new_transaction.add_item(param,qty,try_alt)

    print(new_transaction)

    # update live inventory
    inv.process_transaction(new_transaction)

    # add to transaction record
    for tr in transaction_records:
        tr.add_transaction(new_transaction)

    return new_transaction

def cancel_transaction(prev_t, inv):
    if prev_t is None:
        print("ACHTUNG: Nothing to CANCEL. (Note: can only cancel transcations)")
    else:
        print("UNDOING THIS:",prev_t)
        prev_t.cancelled = True
        inv.undo_transaction(prev_t)

if __name__ == '__main__':
    main()
