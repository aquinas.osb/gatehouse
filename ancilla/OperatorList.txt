Operator Command Thingies


Prefix characters: "	Marks a main operator, such as a sale or a completion of a sale. 
¬	Marks a subordinate operator. 
Dates: Dates will generally by given as suffixes, such as:"BeginGStocktake	 2021/07/23 23:20:44


"SALE	Records Sale in Gatehouse
"CANCEL	Cancels entire transaction from operator (such as "SALE)
¬Delete-Previous	(Deletes previous item [default] or items if number follows, e.g. 

¬Delete-Previous	
¬200	
deletes the 200 previous items back to the most recent operator such as "SALE	

Is it possible to make: ¬Delete-Previous	
¬Delete-Previous	
delete 2 items the same as: ¬Delete-Previous	
¬2	
?)

"Stock-to-Gatehouse	Notes that stock should be taken off of online stock and put into Gatehouse stock. 
"Stock-to-Online	Notes that stock should removed from Gatehouse inventory and added to online inventory.
"PURCHASE	Adds new stock to online inventory. All stock is added to online by default. Gatehouse stock is then transfered from online
"GIVENAWAY	Notes that stock was not sold, but was given away free of charge or binned.
"Stock-to-Library	Notes that stock was not sold, but was transfered to the Monastery Library.
¬ProductNote	If one cannot find the barcode, this us to identify what product was sold via the date and time noted. For instance: ¬ProductNote	 2021/07/23 23:04:18 tells us to look for a written record dated 2021/07/23 23 o'clock.  
¬PriceNote	As with ¬ProductNote, except that it notes a price lower than the posted price. 
¬ByEnvelope	As with ¬ProductNote, except date and time are written on the envelope. 
"BeginGStocktake	What it sounds like.
"PauseGStocktake	Allows for something else to happen -- e.g. a sale -- during the middle of a stocktake. 
"ResumeGStocktake	Continues stocktake from before the operator "PauseGStocktake	
"EndGStocktake	Makes end of a stocktake. 
"Complete	Except for Stocktake operators, "Complete marks the end of any operator that begins with a "

The following numbers get tacked on to the operator above as quantity. 
¬1	
¬2	
¬3	
¬4	
¬5	
¬6	
¬7	
¬8	
¬9	
¬10	
¬11	
¬12	
¬13	
¬14	
¬15	
¬20	
¬25	
¬30	
¬35	
¬40	
¬75	
¬90	
¬100	
¬200	
¬450	
¬500	
¬750	





 







