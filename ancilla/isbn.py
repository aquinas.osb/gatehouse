"""
isbn.py - submodule to look up ISBN info from openlibrary.org
"""

import requests
import json

def _get_api_info(k):
    response = requests.request('GET',
                                'https://openlibrary.org%s.json' % k,
                                json=None,
                                headers={"Accept": "application/json",})

    text = response.text
    if response.status_code != 200:
        text = {'error':response.status_code}
    else:
        try:
            text = json.loads(text)
        except:
            pass
    return text

def get_isbn_info(isbn):
    return _get_api_info('/isbn/%s' % isbn)

def get_name_of_first_author(list_of_authors):
    author_data = _get_api_info(list_of_authors[0]['key'])
    return (str(author_data['error']) if 'error' in author_data else author_data.get('name','!key error!'))
