"""
cor.py -- Cor librarii Ancillae.
"""

_version = "v0.6.1"

import sys
from collections import OrderedDict, namedtuple

import openpyxl

from ancilla import xl

from ancilla.config import *

shell_splash = """
--------------------------------
======== Ancilla %s ========
"Fiat mihi secudum verbum tuum!"
--------------------------------
""" % _version

# within this library these labels are arbitrary, but in pub_cycpher_lector, they determine the letter used in corresponding the stock take commands.
gatehouse_stock_type = 'G'
online_stock_type    = 'O'

NO_TAX_RATE = 0.0
IE_TAX_RATE = 0.23
IE_HALF_TAX_RATE = 0.13

class product_info:
    def  __init__(self,code,code_alt,aronium_sku,title,author=None,price_base=None,price_with_tax=None,tax_rate=NO_TAX_RATE):
        self.code = code
        self.code_alt = code_alt
        self.aronium_sku = aronium_sku
        self.title = title
        self.author = author
        self.price_base = price_base
        self.price_with_tax = price_with_tax
        self.tax_rate = tax_rate

        self.xl_sheet = None
        self.xl_row = None

    def __repr__(self):
        return 'product[%s]' % ','.join([repr(self.code),
                                         repr(self.code_alt),
                                         repr(self.aronium_sku),
                                         repr(self.title),
                                         repr(self.author),
                                         repr(self.price_base),
                                         repr(self.price_with_tax),
                                         repr(self.tax_rate)])
product_db = {}
product_alt_mapping = {}

class inventory_item:
    def __init__(self,product, qty_gstock=0, qty_istock=0, qty_purchased=0, qty_sold=0, qty_given_away=0):
        self.product = product
        self.qty_stock = {gatehouse_stock_type:qty_gstock, online_stock_type:qty_istock}
        self.qty_purchased = qty_purchased
        self.qty_sold = qty_sold
        self.qty_given_away = qty_given_away

    def __repr__(self):
        return 'inventory_item[%s]' % ','.join([repr(self.product),
                                                repr(self.qty_stock),
                                                repr(self.qty_purchased),
                                                repr(self.qty_sold),
                                                repr(self.qty_given_away)])

class inventory:
    def __init__(self, filename=None, xl_specs=None):
        self.items = {}
        self.filename = filename

        self.xl_specs = xl_specs
        self.xl_col_idxs = {}
        self.xl_add_item_at = None

    @property
    def all_items(self):
        return self.items.values()

    def _add_new_item(self, code):
        if code not in self.items:
            p = product_db.get(code)
            if p is None:
                p = product_info(code,None,None,None)
                product_db[code] = p
            self.items[code] = inventory_item(p)

    def stock_take_add(self, code, stock_type):
        code = product_alt_mapping.get(code,code)
        self._add_new_item(code)
        self.items[code].qty_stock[stock_type] += 1
        return self.items[code]

    def add_from_file(self,
                      sheet,
                      row,
                      code,
                      author,
                      title,
                      base_price,
                      tax_rate,
                      qty_gstock,
                      qty_istock,
                      qty_purchased,
                      qty_sold,
                      qty_given_away,
                      code_alt,
                      aronium_sku):

            if code_alt is not None:
                code_alt = code_alt.strip()
                if len(code_alt) < 1:
                    code_alt = None
            if code in product_db or code in product_alt_mapping:
                print("ACHTUNG: Duplicate barcode. taking first only:",code)
                return -1
            elif code_alt is not None and (code_alt in product_db or code_alt in product_alt_mapping):
                print("ACHTUNG: Duplicate secondary barcode. taking first only:",code_alt)
                return -1
            else:
                try:
                    base_price = float(base_price)
                except:
                    pass
                p = product_info(code,
                                 code_alt,
                                 aronium_sku,
                                 title,
                                 author,
                                 base_price,
                                 tax_rate=(IE_TAX_RATE if len(tax_rate) > 0 else NO_TAX_RATE))
                p.xl_sheet = sheet
                p.xl_row   = row

                product_db[code] = p
                if code_alt is not None:
                    product_alt_mapping[code_alt] = code

                try:
                    qty_gstock = int(qty_gstock)
                except:
                    qty_gstock = 0
                try:
                    qty_istock = int(qty_istock)
                except:
                    qty_istock = 0
                try:
                    qty_purchased = int(qty_purchased)
                except:
                    qty_purchased = 0
                try:
                    qty_sold = int(qty_sold)
                except:
                    qty_sold = 0
                try:
                    qty_given_away = int(qty_given_away)
                except:
                    qty_given_away = 0
                self.items[code] = inventory_item(p,
                                                  qty_gstock,
                                                  qty_istock,
                                                  qty_purchased,
                                                  qty_sold,
                                                  qty_given_away)
                return 0

    def update_from_stock_take(self, stock_inv, stock_type, _but_actually_this_stock_type=None):
        if _but_actually_this_stock_type is None:
            _but_actually_this_stock_type = stock_type
        if not stock_take_incrementally:
            for i in self.all_items:
                i.qty_stock[stock_type] = 0
        for i in stock_inv.all_items:
            self._add_new_item(i.product.code)
            self.items[i.product.code].qty_stock[stock_type] += i.qty_stock[_but_actually_this_stock_type]

    supported_transactions = [
        'PURCHASE',
        'SALE',
        'GIVENAWAY',
        'Stock-to-Library',
        'Stock-to-Gatehouse',
        'Stock-to-Online',
    ]

    def process_transaction(self, transaction_to_process, update_qty_stock=True, scale=1):

        for code,qty in transaction_to_process.items.items():
            self._add_new_item(code)
            qty = qty * scale

            if transaction_to_process.type == 'PURCHASE':
                self.items[code].qty_purchased += qty
                if update_qty_stock:
                    self.items[code].qty_stock[online_stock_type] += qty

            elif transaction_to_process.type == 'SALE':
                self.items[code].qty_sold += qty
                if update_qty_stock:
                    self.items[code].qty_stock[gatehouse_stock_type] -= qty

            elif transaction_to_process.type in ('GIVENAWAY','Stock-to-Library'):
                self.items[code].qty_given_away += qty
                if update_qty_stock:
                    self.items[code].qty_stock[gatehouse_stock_type] -= qty

            elif transaction_to_process.type == 'Stock-to-Gatehouse':
                if update_qty_stock:
                    self.items[code].qty_stock[online_stock_type] -= qty
                    self.items[code].qty_stock[gatehouse_stock_type] += qty

            elif transaction_to_process.type == 'Stock-to-Online':
                if update_qty_stock:
                    self.items[code].qty_stock[gatehouse_stock_type] -= qty
                    self.items[code].qty_stock[online_stock_type] += qty

            else:
                pass # not implemented

    def undo_transaction(self, transaction_to_process, update_qty_stock=True, scale=1):
        # process transaction with negative quantities in order to
        # subtract whatever was added in process_transaction, add back whatever was subtracted
        self.process_transaction(transaction_to_process, update_qty_stock, (-scale))

    def get_or_add_item_by_aronium_sku(self,sku,title):

        #try to find already in iventory
        for i in self.all_items:
            if i.product.aronium_sku == sku:
                return i

        # or try to find in product db
        for p in product_db.values():
            if p.aronium_sku == sku:
                # add
                i = inventory_item(p)
                self.items[p.code] = i
                return i

        #or make new product
        new_code = "ArSKU_%s" % sku
        p = product_info(new_code,None,sku,title)
        i = inventory_item(p)
        product_db[new_code] = p
        self.items[new_code] = i
        return i

def load_live_inventory(inventory_file):

    print("loading live inventory:",inventory_file)
    wb = xl.load_workbook(inventory_file)

    # load xl_specs for sheet ancilla_spec (but still support pcl_spec)
    table = xl.read_sheet_table(wb, 'ancilla_spec' if 'ancilla_spec' in wb.sheetnames else 'pcl_spec')
    col_len = table.headers.index(':PAX:')
    ancilla_spec_labels = namedtuple('xl_spec', table.headers[:col_len])

    xl_sheet_specs = []
    for r in table.data:
        if r[table.headers[0]] == ':PAX:':
            break # last row
        else:
            xl_sheet_specs.append(ancilla_spec_labels(**{ k: r[k] for k in ancilla_spec_labels._fields }))

    new_live_inventory = inventory(filename=inventory_file, xl_specs=xl_sheet_specs)

    # clear all global product info so that we can reload it
    product_db.clear()
    product_alt_mapping.clear()

    for sheet_spec in new_live_inventory.xl_specs:

        sheet_data = [r for r in wb[sheet_spec.sheet_name].rows]

        # get column indices: (this will break the program if a column is missing from the ancilla_spec sheet; which is fine for now)
        idxs = {
            sheet_spec.code_col:-1,
            sheet_spec.codealt_col:-1,
            sheet_spec.author_col:-1,
            sheet_spec.title_col:-1,
            sheet_spec.baseprice_col:-1,
            sheet_spec.taxrate_col:-1,
            sheet_spec.gstock_col:-1,
            sheet_spec.istock_col:-1,
            sheet_spec.purchased_col:-1,
            sheet_spec.sales_col:-1,
            sheet_spec.giveaway_col:-1,
            sheet_spec.aronium_sku_col:-1,
        }
        label_row = int(sheet_spec.label_row)
        for i,c in enumerate(sheet_data[label_row-1]):
            for k,v in idxs.items():
                if str(c.value).strip() == k:
                    idxs[k] = i

        # and double-check them:
        for k,v in idxs.items():
            if v < 0:
                print("FATAL ERROR: file:",inventory_file)
                print("FATAL ERROR: format not what expected for sheet:",sheet_spec.sheet_name)
                print("FATAL ERROR: in row",label_row,"missing column:",k)
                sys.exit(1)

        new_live_inventory.xl_col_idxs[sheet_spec.sheet_name] = idxs

        # read data from relavent columns
        row_num = label_row
        for row in sheet_data[label_row:]:
            code, codealt, aronium_sku = None, None, None
            try:
                code        = xl.get_cell_val(row, idxs[sheet_spec.code_col], int)
            except:
                pass
            try:
                codealt     = xl.get_cell_val(row, idxs[sheet_spec.codealt_col], int)
            except:
                pass
            try:
                aronium_sku = xl.get_cell_val(row, idxs[sheet_spec.aronium_sku_col], int)
            except:
                pass
            try:
                author      = xl.get_cell_val(row, idxs[sheet_spec.author_col])
                title       = xl.get_cell_val(row, idxs[sheet_spec.title_col])
                baseprice   = xl.get_cell_val(row, idxs[sheet_spec.baseprice_col], float)
                taxrate     = xl.get_cell_val(row, idxs[sheet_spec.taxrate_col])
                gstock      = xl.get_cell_val(row, idxs[sheet_spec.gstock_col], int)
                istock      = xl.get_cell_val(row, idxs[sheet_spec.istock_col], int)
                purchased   = xl.get_cell_val(row, idxs[sheet_spec.purchased_col], int)
                sales       = xl.get_cell_val(row, idxs[sheet_spec.sales_col], int)
                giveaway    = xl.get_cell_val(row, idxs[sheet_spec.giveaway_col], int)
            except:
                pass

            if aronium_sku is not None and len(aronium_sku) > 0:
                if code is None or len(code) < 1:
                    code = "ArSKU_%s" % aronium_sku

            if code is not None and len(code) > 0:
                rc = new_live_inventory.add_from_file(sheet_spec.sheet_name,
                                                      row_num,
                                                      code,
                                                      author,
                                                      title,
                                                      baseprice,
                                                      taxrate,
                                                      gstock,
                                                      istock,
                                                      purchased,
                                                      sales,
                                                      giveaway,
                                                      codealt,
                                                      aronium_sku)
            row_num += 1

        # specify which row to add new items to in excel (but only for the first sheet)
        if new_live_inventory.xl_add_item_at is None:
            new_live_inventory.xl_add_item_at = row_num

    return new_live_inventory

def save_live_inventory(live_inventory, inventory_file=None):
    if inventory_file is None:
        inventory_file = live_inventory.filename

    #open workbook for writing
    print("Saving live inventory:",inventory_file)
    wb = xl.load_workbook(inventory_file, read_only=False, data_only=False)

    #update affected quantities, adding new items if needed
    xl_specs_by_name = {s.sheet_name: s for s in live_inventory.xl_specs}
    for item in live_inventory.all_items:
        xl_code  = None
        xl_arsku = None
        xl_title = None
        xl_sheet = item.product.xl_sheet
        xl_row   = item.product.xl_row

        if None in (xl_sheet, xl_row):
            # new item
            xl_code  = item.product.code
            xl_arsku = item.product.aronium_sku
            xl_title = item.product.title
            xl_sheet = live_inventory.xl_specs[0].sheet_name
            xl_row   = live_inventory.xl_add_item_at
            live_inventory.xl_add_item_at += 1
            print(":: Adding new item to",xl_sheet,"; code:",xl_code,"; row:",xl_row)

        spec = xl_specs_by_name[xl_sheet]
        idxs = live_inventory.xl_col_idxs[xl_sheet]

        ws = wb[xl_sheet]
        if xl_code is not None:
            xl.update_cell_val(ws, xl_row, idxs[spec.code_col],        xl_code)
        if xl_arsku is not None:
            xl.update_cell_val(ws, xl_row, idxs[spec.aronium_sku_col], xl_arsku)
        if xl_title is not None:
            xl.update_cell_val(ws, xl_row, idxs[spec.title_col],       xl_title)

        xl.update_cell_val(ws, xl_row, idxs[spec.gstock_col],    item.qty_stock[gatehouse_stock_type])
        xl.update_cell_val(ws, xl_row, idxs[spec.istock_col],    item.qty_stock[online_stock_type])
        xl.update_cell_val(ws, xl_row, idxs[spec.purchased_col], item.qty_purchased)
        xl.update_cell_val(ws, xl_row, idxs[spec.sales_col],     item.qty_sold)
        xl.update_cell_val(ws, xl_row, idxs[spec.giveaway_col],  item.qty_given_away)

    wb.save(inventory_file)

class transaction:

    def __init__(self, _type, _timestamp=None):
        self.type       = _type
        self.timestamp  = _timestamp
        self.items      = OrderedDict()
        self.cancelled  = False

    def add_item(self, code, qty=1, try_alt=False):

        if try_alt:
            code = product_alt_mapping.get(code,code)  # replace any scanned barcode with the primary barcode if possible

        if code not in self.items:
            self.items[code] = 0
        self.items[code] += qty

    def __str__(self):
        r = "::%s @ %s" % (self.type, str(self.timestamp))
        if self.cancelled:
            r = "CANCELLED{%s}" % r
        for code,qty in self.items.items():
            r = r + "\n::  %s * %s" % (str(qty),str(code))
        return r

    def __len__(self):
        return len(self.items)

FILE_STRFTIME = '%Y_%m_%d_%H_%M_%S'

class transaction_record:

    def __init__(self, fname, type_filter=None, report_price_data=True):
        self.fname = fname
        self.data = []
        self.type_filter = type_filter
        self.report_price_data = report_price_data

    def add_transaction(self, t):
        if self.type_filter is None or self.type_filter == t.type:
            self.data.append(t)

    def load_from_file(self): 
        pass # TODO

    def save_file(self):

        if len(self.data) > 0:

            # create report
            wb, ws = xl.new_workbook("Report")

            current_row = [1]
            def _write_row(r,is_header=False):
                for c in range(1,1+len(r)):
                    current_cell = ws.cell(column=c, row=current_row[0], value=r[c-1])
                    if is_header:
                        current_cell.font = openpyxl.styles.Font(bold=True)
                current_row[0] += 1
                return current_cell # last cell

            # create headers
            csv_headers = ["Transact.","time","Qty","barcode","Title","Author"]
            if self.report_price_data:
                csv_headers = csv_headers + ["Product Price","Line Price","Tax Rate","Line Price inc Tax","Total"]
            line_price_formula = "=C%i*G%i" # qty * price_base
            line_price_inc_tax_formula = "=H%i+(H%i*I%i)" # line_price + (line_price * tax_rate)
            _write_row(csv_headers,True)

            # populate rows
            for t in self.data:
                first = True
                total = 0.0
                for itemcode,itemqty in t.items.items():

                    new_row = ["",""]
                    if first:
                        _type = t.type
                        if t.cancelled:
                            _type = "CANCELLED{%s}" % _type
                        new_row = [_type,t.timestamp]

                    p = None
                    _title,_author = "",""
                    if itemcode in product_db:
                        p = product_db[itemcode]
                        _title  = p.title
                        _author = p.author
                    new_row = new_row + [itemqty, itemcode, _title, _author]

                    if self.report_price_data and not t.cancelled:
                        new_row = new_row + [('!fixme' if p is None else p.price_base),
                                             line_price_formula % (current_row[0],current_row[0]),
                                             ('!fixme' if p is None else p.tax_rate),
                                             line_price_inc_tax_formula % (current_row[0],current_row[0],current_row[0]),
                                             None]

                    update_total_cell = _write_row(new_row)
                    if first:
                        total_cell = update_total_cell
                        first = False

                    total_formula_letter = openpyxl.utils.get_column_letter(total_cell.col_idx - 1)
                    total_cell.value     = "=SUM(%s%i:%s%i)" % (total_formula_letter,
                                                                total_cell.row,
                                                                total_formula_letter,
                                                                current_row[0]-1)

            # create name and save
            first_timestamp = self.data[0].timestamp
            last_timestamp = self.data[-1].timestamp

            file_timestamp = "unknowntime"
            if first_timestamp is not None and last_timestamp is not None:
                file_timestamp = "%s_to_%s" % (first_timestamp.strftime(FILE_STRFTIME),
                                               last_timestamp.strftime(FILE_STRFTIME))
            elif first_timestamp is not None:
                file_timestamp = "from_%s" % first_timestamp.strftime(FILE_STRFTIME)
            elif last_timestamp is not None:
                file_timestamp = "upto_%s" % last_timestamp.strftime(FILE_STRFTIME)

            filename = "%s_%s.xlsx" % (self.fname,file_timestamp)

            print("Saving record:", filename)
            wb.save(filename = filename)

        else:

            print("Empty record:",self.fname,"(Nothing to Save)")

def load_transaction_records():

    # just create new for now
    sales_record = transaction_record("sales_record",
                                      type_filter="SALE")
    stock_record = transaction_record("stock_transactions",
                                      report_price_data=False)
    return sales_record, stock_record
