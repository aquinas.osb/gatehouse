"""
args.py -- submodule to handle command arguments and file input
"""

import argparse

from tkinter import Tk
from tkinter.filedialog import askopenfilename

class AncillaParser(argparse.ArgumentParser):

    def __init__(self, input_file_filter=None, intro_callback=None, use_default_args=True):
        super(AncillaParser, self).__init__()

        self.input_file_filter = input_file_filter
        self.file_chooser_args = []
        self.file_chooser_overridden_by = {}
        self.intro_callback = intro_callback
        self.did_intro = False

        # default Ancilla arguments
        if use_default_args:

            self.add_file_chooser_argument('--inventory', '-n',
                    help='name of the inventory file (e.g. <2022_Stocklist.xlsx>)',
                    file_chooser_title="INVENTORY",
                    file_chooser_filter=(('Live Inventory File (*stocklist*.xlsx)','*stocklist*.xlsx'),))

            self.add_file_chooser_argument('input_files', nargs='*',
                    help='Input file(s) to be processed (e.g. <June_2022.txt>)',
                    file_chooser_title="INPUT DATA",
                    file_chooser_filter=self.input_file_filter)

            self.add_argument('--dry-run', action='store_true',
                    help='Don\'t save any changes to the inventory file.')

    def add_file_chooser_argument(self, *args, **kwargs):

        file_chooser_title  = _kwarg_pop(kwargs, 'file_chooser_title')
        file_chooser_filter = _kwarg_pop(kwargs, 'file_chooser_filter')

        action = self.add_argument(*args, **kwargs)

        self.file_chooser_args.append((action,
                                       file_chooser_title,
                                       file_chooser_filter))

    def add_argument_which_overrides_file_chooser(self, *args, **kwargs):
        # I hope this isn't too convoluted. this is a somewhat ad-hoc way of getting out of the corner that I've coded myself into.

        overrides_arg  = _kwarg_pop(kwargs, 'overrides_arg')

        action = self.add_argument(*args, **kwargs)

        self.file_chooser_overridden_by[overrides_arg] = action

    def parse_args(self, args=None, namespace=None):

        args = super(AncillaParser, self).parse_args(args,namespace)
        args = vars(args) # convert to dict

        # after parsing the arguments, check if any of the arguments need to supply a file chooser
        def _choose_file_with_intro(file_chooser_title, file_chooser_filter):
            if not self.did_intro and self.intro_callback is not None:
                self.intro_callback()
                self.did_intro = True
            return choose_file(file_chooser_title, file_chooser_filter)

        for action, file_chooser_title, file_chooser_filter in self.file_chooser_args:

            overriding_action = self.file_chooser_overridden_by.get(action.dest)
            if (overriding_action is not None) and (args[overriding_action.dest]):
                continue

            if action.nargs == '*':
                if len(args[action.dest]) == 0:
                    args[action.dest].append(_choose_file_with_intro(file_chooser_title, file_chooser_filter))
            else:
                if args[action.dest] is None:
                    args[action.dest] = _choose_file_with_intro(file_chooser_title, file_chooser_filter)

        # set flag for did_intro with obtuse name that wont confict with potential arguments
        args['yes_we_did_do_the_intro'] = self.did_intro

        return argparse.Namespace(**args) # convert back to Namespace

def _kwarg_pop(_kwargs, kw):
    '''used to get and remove kwargs not expected by add_argument()'''
    if kw in _kwargs:
        return _kwargs.pop(kw)
    return None

def choose_file(msg, filetypes):
    print("CHOOSE",msg)
    Tk().withdraw()
    return askopenfilename(title="CHOOSE "+msg, filetypes=filetypes)
