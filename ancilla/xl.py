"""
xl.py -- submodule to handle Excel files
"""

import openpyxl

def load_workbook(f, **kwargs):

    # default kwargs to pass through
    for kw, default in (('read_only', True),
                        ('data_only', True)):
        if kw not in kwargs:
            kwargs[kw] = default

    return openpyxl.load_workbook(f, **kwargs)

def new_workbook(sheetname = None):

    wb = openpyxl.Workbook()
    ws = wb.active
    if sheetname is not None:
        ws.title = sheetname
    return wb, ws

def get_cell_val(r,c,type_cast=None):

        v = r[c].value
        if type_cast is not None:
            try:
                v = type_cast(v)
            except:
                pass
        return str(v).replace("None","").strip()

def update_cell_val(ws, r, c, v):
    ws["%s%s" % (openpyxl.utils.get_column_letter(c+1),r+1)] = v

def bool_val(v):

    if type(v) == type(''):
        v = v.lower()
        return v.startswith('t') or v.startswith('s')

    return bool(v)

class SheetTable:

    def __init__(self,title, headers=None):
        self.title = title
        self.headers = headers
        self.data = []

    def append(self, r):
        self.data.append(r)

    @property
    def next_row_num(self):
        return 2 + len(self.data)

def read_sheet_table(wb, sheetname):

    table = SheetTable(sheetname)

    for r in wb[sheetname].rows:
        line  = [str(c.value) for c in r]
        if table.headers is None:
            table.headers = line
        else:
            table.append(dict(zip(table.headers, line)))

    return table

def load_workbook_tables(f):

    print("Loading Excel workbook:",f)
    wb = load_workbook(f)
    return [read_sheet_table(wb, name) for name in wb.sheetnames]

def save_workbook_tables(tables, f):

    print("Saving Excel workbook:", f)
    wb, ws = new_workbook()

    del_default = True
    for table in tables:

        if table.title == ws._default_title:
            del_default = False

        if table.title in wb.sheetnames:
            ws = wb[table.title]
        else:
            ws = wb.create_sheet(table.title)

        ws.append(table.headers)

        for row in table.data:

            if type(row) == type({}):
                row = [row.get(x) for x in table.headers]
            else:
                row = list(row)

            ws.append(row)

    if del_default:
        del wb[ws._default_title] # remove default sheet if not needed

    wb.save(f)
