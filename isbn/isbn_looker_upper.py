
import os, sys
import csv
import re
import time
from collections import OrderedDict

import ancilla

def process_list(isbn_list):
    headers = ['ISBN','No.','Author Name','title']
    rows = OrderedDict()

    QTY_MARKER = 'Â¬'
    prev_isbn = None
    prev_line_was_isbn = False
    for line in load_isbns(isbn_list):
        if len(line) > 0:
            print(line)
            msg = ''
            if line.startswith(QTY_MARKER):
                qty = int(line.replace(QTY_MARKER,'').strip()) # TODO: handle malformed int
                if prev_line_was_isbn: qty -= 1 # to account for the first qty being added with ISBN itself
                if prev_isbn is not None:
                    prev_isbn['No.'] += qty
                    msg = 'Adding %s more of %s' % (qty, prev_isbn['ISBN'])
                else:
                    msg = 'ACHTUNG: Ignoring this line becasue no ISBN is specified.'
                prev_line_was_isbn = False
            else:
                if line in rows:
                    data = rows[line]
                    data['No.'] += 1
                else:
                    data = ancilla.isbn.get_isbn_info(line)
                    for k in data.keys(): 
                        if k not in headers:
                            headers.append(k)
                    data['ISBN'] = line
                    data['No.']  = 1
                    if 'error' not in data and 'authors' in data:
                        data['Author Name'] = ancilla.isbn.get_name_of_first_author(data['authors'])
                    rows[line]   = data
                prev_isbn = data
                prev_line_was_isbn = True
                msg = data['error'] if 'error' in data else 'OK!'
            print('->',msg)

    write_csv_table(os.path.splitext(isbn_list)[0] + '.csv', headers, rows)

def intro():
    print(ancilla.cor.shell_splash)
    print('------------------------')
    print(' ISBN LOOKER UPPER v0.3 ')
    print('------------------------')
    print('')
    time.sleep(1)
    print('woah woah woah! Are you ready?? In order to use this program you need to make a txt file with all your ISBNs in it. one on each line.  Did you make that file yet? okay. Once we get going it will ask you to select a file.  You have to select the file you made with all your ISBNs in it.  After that, the program will do its thing and then create a new file next to it which can be opened with all your excell data.  Does that make sense?  Okay then...')
    os.system('pause')
    time.sleep(1)
    print('Are you sure? did you actually read that text? okay then...')
    os.system('pause')

def outro():
    time.sleep(.5)
    print('...')
    time.sleep(.5)
    print('...')
    time.sleep(.5)
    print('...')
    time.sleep(.5)
    print('*HACKER VOICE* "I\'m in."')
    time.sleep(1)
    os.system('pause')

date_re = re.compile(r'[0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]')
def load_isbns(filename):
    r = []
    with open(filename,'r') as f:
        r = [date_re.sub('',l).strip() for l in f.readlines()]
    return r

def write_csv_table(filename, headers, rows):
    UNICODE_INDICATOR = '\ufeff'
    with open(filename,'w', newline='', encoding='utf-8') as f:
        w = csv.writer(f)
        w.writerow([UNICODE_INDICATOR+headers[0]] + headers[1:])
        for k, v in rows.items():
            w.writerow([str(v.get(h,'')) for h in headers])

def main():

    parser = ancilla.args.AncillaParser(intro_callback=intro, use_default_args=False)

    parser.add_file_chooser_argument('input_file', nargs='?',
            help='Input file to be processed (e.g. <ISBNs.txt>)',
            file_chooser_title="INPUT DATA",
            file_chooser_filter=self.input_file_filter)

    args = parser.parse_args()

    process_list(args.input_file)

    if args.yes_we_did_do_the_intro:
        outro()
